var app = angular.module('flat',[]);

Number.prototype.toFix = function () {
	var v = this.toFixed(2),
		idx = v.indexOf('.'),
		tmp = '';

	if(v.length<=6)
		return v;
		
	for(var i=1,l=v.length; i<=l; i++) {
		if(i>6 && (i-1)%3 == 0) {
			tmp = ' ' + tmp;
		}
		tmp = v[l-i] + tmp;
	}
	return tmp;
}

app.controller('main', function($scope){
	var s = $scope;
	// вводимые параметры
	s.input = {
		crSum: 0, // сумма кредита
		crYears: 0, // на сколько лет
		crDefPrecents: 0, // начальная процентная ставка

		save: 0, // ежемесчный вклад
		price: 0, // стоимость квартиры
		flatGrow: 0, // прирост стоимости квартиры в процентах
		rent: 0, // затраты на съём жилья
		savePerc: 0, // процент по вкладу
	};

	// установить параметры по умолчанию
	s.setDef = function () {
		s.input.crSum = 5000000;
		s.input.crYears = 5;
		s.input.crDefPrecents = 10.5;

		s.input.save = 60000;
		s.input.price = 5000000;
		s.input.flatGrow = Math.floor(((4.43+0.8)/2)*10)/10;
		s.input.rent = 20000;
		s.input.savePerc = 8;
	}

	// показать всё
	s.ok = false;
	s.saveCr = 0;

	s.calc = function () {
		s.ok = true;
		// массив данных
		s.data = [];

		var months = s.input.crYears * 12,
			// ежемесячная плата
			monthlyPay = +s.input.crSum / months;
			// долг
			crLeft = s.input.crSum;

		for(var i=0;i<months; i++) {
			var obj = {};

			// номер месяца
			obj.month = i+1;
			// годовой процент
			obj.yearPerc = s.input.crDefPrecents; 
			// ежемесячный процент 
			obj.monthlyPerc= s.input.crDefPrecents / 12;
			// ежемесячный платёж долга
			obj.monthlyPay = monthlyPay;
			// остаток выплаты на начало месяца
			obj.creditLeftBegin = i == 0 ? crLeft : s.data[i-1].creditLeftEnd;
			// сколько осталось кредита
			obj.creditLeftEnd = (obj.creditLeftBegin - obj.monthlyPay) < 0 ? 0 : (obj.creditLeftBegin - obj.monthlyPay);
			// ежемесячный платёж процентов
			obj.monthlyPayPerc = (s.input.crDefPrecents / 1200) * obj.creditLeftBegin;
			// сумма ежемесячного платежа
			obj.mothlyTotalyPay = obj.monthlyPay + obj.monthlyPayPerc;
			// выплачено кредита
			obj.payed = s.input.crSum - obj.creditLeftEnd;
			// процент закрытия кредита
			obj.payedPercents = (obj.payed / s.input.crSum) * 100;


			crLeft -= monthlyPay;
			crLeft = crLeft < 0 ? 0 : crLeft;
			

			/// расчёт накопления

			// сбережения
			obj.saved = s.input.save + ( i==0 ? 0 : s.data[i-1].saved);
			// изменение стоимости квартиры
			obj.flatPrice = (i==0 ? s.input.price : s.data[i-1].flatPrice) * (1+s.input.flatGrow/1200);
			// изменение стоимости жилья относительно начальной
			obj.flatPriceDelta = obj.flatPrice - s.input.price;
			// затраты на аренду жилья
			obj.rent = s.input.rent +  ( i==0 ? 0 : s.data[i-1].rent);
			// начисление %
			if(obj.month>23 && obj.month % 12 == 0)
				obj.saved += s.data[i-12].saved * s.input.savePerc/100

			// решение о возможности покупки квартиры
			if(obj.saved >= obj.flatPrice) {
				obj._buy = obj.flatPrice + obj.rent;
			}

			s.data.push(obj);
		}

		s.saveCr = +s.input.crSum;		
	}

	// выплаченные проценты
	s.payedPercents = function () {
		if(!s.data)
			return '0';
		var x = 0;
		s.data.forEach(d=>x+=d.monthlyPayPerc);
		return x;
	}
	// выплачено всего
	s.payedTotal = function () {
		if(!s.data)
			return '0';
		var x = 0;
		s.data.forEach(d=>x+=d.mothlyTotalyPay);
		return x;
	}
	// средний платёж
	s.avgPay = function () {
		if(!s.data)
			return '0';
		var x = 0;
		s.data.forEach(d=>x+=d.mothlyTotalyPay);		
		return (x/s.data.length) - s.input.rent;
	}

	// заключение о покупке
	s.flatBuy = function () {
		if(s.data)
			for(var i =0; i < s.data.length; i++) {
				var x = s.data[i];
				if(x._buy)
					return `Покупка на ${x.month} месяце; затраты ${x._buy.toFix()}`;
			}
		return 'Накопить не удалось';
	}
});